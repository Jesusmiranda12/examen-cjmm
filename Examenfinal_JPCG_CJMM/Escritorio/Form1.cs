﻿using System;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading.Tasks;
using System.Json;
using System.Net;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;


namespace Escritorio
{
    public partial class Form1 : Form
    {
        public string carpeta;
        public OpenFileDialog _archivo;
        public Form1()
        {
            InitializeComponent();
            _archivo = new OpenFileDialog();
        }
        private async void descargarImagen()
        {

            string carpeta = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyPictures);
            string archivoLocal = txtBuscar.Text + ".jpg";
            string ruta = System.IO.Path.Combine(carpeta, archivoLocal);
            var CuentadeAlmacenamiento = CloudStorageAccount.Parse
            ("DefaultEndpointsProtocol=https;AccountName=practicas21;AccountKey=RvRg5OkRH1mDxjYPr9BKYaTCyNHDMZCtNgxZCW1qeEwcZ2JiaJGdiaUyKJW/YcuQ7Eq0mHUDmmspFyX8Cfbtxw==;EndpointSuffix=core.windows.net");
            CloudBlobClient clienteBlob = CuentadeAlmacenamiento.CreateCloudBlobClient();
            CloudBlobContainer contenedor = clienteBlob.GetContainerReference("contenedorcapetillo");
            CloudBlockBlob recursoBlob = contenedor.GetBlockBlobReference(txtBuscar.Text + ".jpg");
            var stream = File.OpenWrite(ruta);
            await recursoBlob.DownloadToStreamAsync(stream);
            stream.Close();
            var variable = carpeta + "/" + archivoLocal;

          
  
            _archivo.FileName = variable;
            string direccion = _archivo.FileName;
            pbFoto.ImageLocation = direccion;
            pbFoto.SizeMode = PictureBoxSizeMode.StretchImage;

            MessageBox.Show("Se ha descargado la imagen en tu carpeta de imagenes");


        }
        public async void buscar()
        {
            string Curp = txtBuscar.Text;
            var API = "https://jpcg.azurewebsites.net/Api/ControladorPrincipal/Consultar?Curp=" + Curp;
            JsonValue json = await Datos(API);
            Transform(json);
            if (int.Parse( txtEdad.Text)<=40)
            {
                lblVacuna.Text = "Vacuna indicada: otro tipo";
            }
            else if(int.Parse(txtEdad.Text)>=50)
            {
                lblVacuna.Text = "Vacuna indicada: Astraseneca";

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buscar();
            descargarImagen();
        }
        public async Task<JsonValue> Datos(string API)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(new Uri(API));
                request.ContentType = "Aplication/json";
                request.Method = "GET";
                using (WebResponse response = await request.GetResponseAsync())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        var jsondoc = await Task.Run(() => JsonValue.Load(stream));
                        return jsondoc;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        public void Transform(JsonValue json)
        {
            try
            {
                var Resultados = json[0];
                txtNombres.Text = Resultados["nombres"].ToString();
                txtApellidos.Text = Resultados["apellidos"].ToString();
                txtEdad.Text = Resultados["edad"].ToString();
                txtPadecimientos.Text = Resultados["enfermedades"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex.Message.ToString());
            }
        }
    }
}
