﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using System.Data;
using System;
using Android.Widget;
using Android.Graphics;
using Plugin.Media;
using System.IO;
using Plugin.CurrentActivity;
using Microsoft.WindowsAzure.Storage;
using System.Net;

namespace AndroidApp
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        string Archivo;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            SupportActionBar.Hide();
            CrossCurrentActivity.Current.Init(this, savedInstanceState);
            var Imagen = FindViewById<ImageView>(Resource.Id.imgFoto);
            var btnAlmacenar = FindViewById<Button>(Resource.Id.btnGuardar);
            var txtnombre = FindViewById<EditText>(Resource.Id.txtNombre);
            var txtcurp = FindViewById<EditText>(Resource.Id.txtCurp);
            var txtapellidos = FindViewById<EditText>(Resource.Id.txtApellidos);
            var txtedad = FindViewById<EditText>(Resource.Id.txtEdad);
            var txtpadecimientos = FindViewById<EditText>(Resource.Id.txtPadecimientos);
            Imagen.Click += async delegate
            {
                await CrossMedia.Current.Initialize();
                var archivo = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                {
                    Directory = "Imágenes",
                    Name = txtcurp.Text,
                    SaveToAlbum = true,
                    CompressionQuality = 30,
                    CustomPhotoSize = 30,
                    PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,
                    DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Rear
                });
                if (archivo == null)
                {
                    return;
                }
                Bitmap bp = BitmapFactory.DecodeStream(archivo.GetStream());
                Archivo = System.IO.Path.Combine(System.IO.Path.Combine
                    (System.Environment.GetFolderPath
                    (System.Environment.SpecialFolder.Personal),
                    txtcurp.Text + ".jpg"));
                var stream = new FileStream(Archivo, FileMode.Create);
                bp.Compress(Bitmap.CompressFormat.Jpeg, 30, stream);
                stream.Close();
                Imagen.SetImageBitmap(bp);
                long memoria1 = GC.GetTotalMemory(false);
                Toast.MakeText(this, memoria1.ToString(), ToastLength.Long).Show();
                GC.Collect();
                long memoria2 = GC.GetTotalMemory(false);
                Toast.MakeText(this, memoria2.ToString(), ToastLength.Long).Show();
            };
            btnAlmacenar.Click += async delegate
            {
                try
                {
                    var CuentadeAlmacenamiento = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=practicas21;AccountKey=RvRg5OkRH1mDxjYPr9BKYaTCyNHDMZCtNgxZCW1qeEwcZ2JiaJGdiaUyKJW/YcuQ7Eq0mHUDmmspFyX8Cfbtxw==;EndpointSuffix=core.windows.net");
                    var ClienteBlob = CuentadeAlmacenamiento.CreateCloudBlobClient();
                    var Carpeta = ClienteBlob.GetContainerReference("contenedorcapetillo");
                    var resourceBlob = Carpeta.GetBlockBlobReference(txtcurp.Text + ".jpg");
                    await resourceBlob.UploadFromFileAsync(Archivo.ToString());

                    var Curp = txtcurp.Text;
                    var Nombre = txtnombre.Text;
                    var Apellidos = txtapellidos.Text;
                    var Edad = int.Parse(txtedad.Text);
                    var Padecimientos = txtpadecimientos.Text;
                    var API = "https://jpcg.azurewebsites.net/Api/ControladorPrincipal/Insertar?Curp=" + Curp + " &Nombres=" + Nombre + " &Apellidos=" + Apellidos + " &Edad=" + Edad + " &Enfermedades=" + Padecimientos + "";
                    var request = (HttpWebRequest)WebRequest.Create(API);
                    WebResponse response = request.GetResponse();
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    string responseText = reader.ReadToEnd();
                    Toast.MakeText(this, responseText.ToString(), ToastLength.Long);
                    Toast.MakeText(this, "Imágen guardada correctamente", ToastLength.Long).Show();

                }
                catch (Exception)
                {
                    throw;
                }
            };
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}